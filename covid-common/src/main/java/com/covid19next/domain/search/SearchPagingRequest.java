package com.covid19next.domain.search;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Getter
@NoArgsConstructor
public class SearchPagingRequest {

    private String keyword;
    @NotNull
    private Integer page;
    @NotNull
    private Integer size;
    @NotNull
    private String sort;

    @Builder
    public SearchPagingRequest(String keyword, Integer page, Integer size, String sort) {
        this.keyword = keyword;
        this.page = page;
        this.size = size;
        this.sort = sort;
    }
}
