FROM openjdk:8-jre-alpine
VOLUME /tmp
EXPOSE 9010
ADD /covid-api/build/libs/*.jar covid_app.jar
ENTRYPOINT ["sh", "-c", "java -Djava.security.egd=file:/dev/./urandom -Duser.country=KR -Duser.language=ko -Duser.timezone=Asia/Seoul -Dspring.profiles.active=api-prod -jar /covid_app.jar"]
