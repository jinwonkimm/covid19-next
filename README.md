## 사용자(회원) 관리
### 소셜 로그인 기능
* 구글 로그인 기능을 지원한다.
* 카카오 로그인 기능을 지원한다.

### 회원 관리 기능
### 회원가입
* 서비스 자체적 회원가입 기능을 지원한다.
* Email, Password, Phone 등의 정보를 입력받는다.

### 로그인 로그 모니터링 페이지
* 회원이 로그인한 기록을 확인한다.

### 회원관리 페이지
* 회원의 정보를 관리 및 모니터링 한다.

## 여행관련 서비스
* 여행가능한 국가 정보 제공 기능
* 여행가능한 국가 정보를 제공한다.
* covid19 시대에 현재 여행 가능한 국가를 확인할 수 있다.
* 여행 가능한 상태로 전환될 유망한 국가 정보 제공 기능
* 여행 가능한 상태로 전환될 유망한 국가 정보를 제공한다.
* 국가별 코로나 뉴스 Notion – The all-in-one workspace for your notes, tasks, wikis, and databases. 

### 여행 가능한 국가의 여행 코스 추천 기능
* 여행이 가능한 국가의 추천 여행 코스(도시) 정보를 제공한다.

### 여행코스 관리 기능
* 사용자 및 관리자가 국가에 대한 여행지를 등록, 수정 한다.

* 여행지에 대해 사용자가 후기 및 별점을 줄 수 있다

### 여행국가, 여행코스 알림 기능
* 구독 기능 선택 사용자에 대해 제공하는 기능
* 여행이 가능하게 전환될 유망한 국가 정보 및 여행 코스를 소개하는 메일, 메시지 발송

### 코로나 관련 기능 (후순위)
* 국내 코로나 정보제공 서비스
* 확진자, 격리자, 사망자 등 코로나 현황 정보를 제공한다.

* 백신 접종률 코로나 거리두기 정보 제공

* 해외 코로나 정보 제공 서비스
* 확진자, 격리자, 사망자 등 코로나 현황 정보를 제공한다.

* 백신 접종률 정보 제공