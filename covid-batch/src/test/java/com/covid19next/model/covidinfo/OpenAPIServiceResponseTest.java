package com.covid19next.model.covidinfo;

import com.covid19next.model.OpenAPIServiceResponse;
import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class OpenAPIServiceResponseTest {



    @Test
    @DisplayName("XML object Text")
    public void xmlMockTest() throws Exception{
        //given
        String errMsg = "test";
        OpenAPIServiceResponse openAPIServiceResponse = new OpenAPIServiceResponse();
        OpenAPIServiceResponse.CmmMsgHeader cmmMsgHeader = new OpenAPIServiceResponse.CmmMsgHeader();
        cmmMsgHeader.setErrMsg(errMsg);
        cmmMsgHeader.setReturnAuthMsg("return Msg");
        //when
        openAPIServiceResponse.setCmmMsgHeader(cmmMsgHeader);
        //then
        Assert.assertEquals(errMsg, openAPIServiceResponse.getCmmMsgHeader().getErrMsg());

    }

}