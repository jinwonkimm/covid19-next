package com.covid19next.service.country;

import com.covid19next.domain.country.Country;
import com.covid19next.repository.county.CountryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

//@SpringBootTest
@ExtendWith(MockitoExtension.class)
class CountryServiceTest {
    @InjectMocks
    CountryService countryService;

    @Mock
    CountryRepository countryRepository;

    @Test
    @DisplayName("Country Service Test")
    public void testFindCountry() throws Exception {
        //given
        String mockCountryCode = "JP";
        List<Country> countries = new ArrayList<>();
        Country country = Country.builder()
                .id(1L)
                .code(mockCountryCode)
                .nameKr("일본")
                .nameEn("Japan")
                .build();
        countries.add(country);

        given(countryRepository.findAll()).willReturn(countries);
        //when
        List<Country> findCountries = countryService.findAllCountries();
        System.out.println(findCountries);
        //then
        Assertions.assertEquals(country.getCode(), findCountries.stream().findFirst().map(Country::getCode).orElse(null));
    }

}